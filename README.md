# Installation instructions
this assumes you have docker desktop, with kubernetes and helm installed

## Add external helm repositories to you local helm setup.
```
helm repo add cnad https://cnad.gitlab.io/helm
```

## Update your local helm repository.
```
helm repo update
```

## End User instructions
```
helm install postgres-operator cnad/postgres-operator
helm install kubemovie cnad/kubemovie
```

That's it, your done. Open up your browser and go to http://localhost.

# Developer instructions 
open cmd and Clone down the git repository
```
git clone https://gitlab.com/cnad/helm.git
```
once installed use helm to install charts
its important that you install postgresop and nignix first or it wont work.
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add cncd https://cnad.gitlab.io/helm
helm install postgres-operator ./charts/postgres-operator
helm install nignix ingress-nginx/ingress-nginx
```
then
```
helm package kubemovie
helm install kubemovie kubemovie
```

## Gitlab results
https://cnad.gitlab.io/helm/